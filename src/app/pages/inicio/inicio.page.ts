import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Componente[] = [
    {
      icon: 'logo-github',
      name: 'Oferta académica',
      redirectTo: '/action-sheet'
    },
    {
      icon: 'ios-calendar',
      name: 'Eventos',
      redirectTo: '/alert'
    },
    {

    icon: 'logo-octocat',
    name: 'Avatar',
    redirectTo: '/avatar'
    },

    {
    icon: 'checkmark-circle-outline',
    name: 'Check-Box',
    redirectTo: '/check-box'
    },

    {
    icon: 'search',
    name: 'Searchbar',
    redirectTo: '/searchbar'
    },


  ]
  ;
  constructor() { }

  ngOnInit() {
  }
}
interface Componente{
  icon: string;
  name: string;
  redirectTo: string;
}

